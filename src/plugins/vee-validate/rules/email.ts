import { REGEXS } from '@/shared/constants/regex';

const config = {
  validate(value: any) {
    return {
      valid: REGEXS.email.test(value),
    };
  },
};

export default config;
