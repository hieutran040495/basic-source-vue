import { REGEXS } from '@/shared/constants/regex';

const config = {
  validate(value: any) {
    return {
      valid: REGEXS.password.test(value),
    };
  },
};

export default config;
