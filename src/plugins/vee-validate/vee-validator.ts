import Vue from 'vue';
import * as VeeValidate from 'vee-validate';
import { configure, extend } from 'vee-validate';
import { required, min, max, confirmed } from 'vee-validate/dist/rules';

import { i18n } from '@/plugins/vue-i18n';
import { format_password, email } from './rules/index';

const config: any = {
  defaultMessage: (field: any, values: any) => {
    // override the field name.
    values._field_ = i18n.t(`fields.${field}`);
    return i18n.t(`validation.${values._rule_}`, values);
  },
};

Vue.use(VeeValidate);

configure(config);

extend('required', required);
extend('min', min);
extend('max', max);
extend('confirmed', confirmed);

// Custom rules
extend('format_password', format_password);
extend('email', email);
