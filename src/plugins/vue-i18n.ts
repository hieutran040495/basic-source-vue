import Vue from 'vue';
import VueI18n from 'vue-i18n';
import axios from 'axios';
import { DEFAULT_LOCALE } from '@/shared/constants/default-locale';
import { ja } from '@/locales/ja';

Vue.use(VueI18n);

export const i18n = new VueI18n({
  locale: DEFAULT_LOCALE,
  fallbackLocale: DEFAULT_LOCALE,
  messages: {
    ja,
  },
});

// const loadedLanguages = [DEFAULT_LOCALE];

function setI18nLanguage(lang: any) {
  i18n.locale = lang;
  axios.defaults.headers.common['Accept-Language'] = lang;
  document.querySelector('html')?.setAttribute('lang', lang);
  return lang;
}

export function loadLanguageAsync(lang: string) {
  // If the same language or the language was already loaded
  // if (i18n.locale === lang || loadedLanguages.includes(lang)) {
  //   i18n.setLocaleMessage(lang, messages);
  //   return Promise.resolve(setI18nLanguage(lang));
  // }

  return import(`@/locales/${lang}.ts`).then((messages) => {
    i18n.setLocaleMessage(lang, messages[lang]);
    // loadedLanguages.push(lang);
    return setI18nLanguage(lang);
  });
}
