import persistedState from 'vuex-persistedstate';
import { LOCAL_STORAGE_KEY } from '@/shared/enums/local-storage';
import SecureLS from 'secure-ls';
const ls = new SecureLS({ isCompression: false });

export default persistedState({
  storage: {
    getItem: (key) => ls.get(key),
    setItem: (key, value) => ls.set(key, value),
    removeItem: (key) => ls.remove(key),
  },
  key: LOCAL_STORAGE_KEY.APP_NAME,
  paths: ['auth'],
});
