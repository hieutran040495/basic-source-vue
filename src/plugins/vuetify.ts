import '@mdi/font/css/materialdesignicons.css';
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
// import 'vuetify/dist/vuetify.min.css';
import Color from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: Color.orange.base,
        secondary: Color.green.darken3,
        accent: Color.blue.accent1,
        error: Color.red.accent2,
        info: Color.lightBlue.accent2,
        success: Color.green.base,
        warning: Color.yellow.base,
      },
    },
  },
  lang: {
    current: 'ja',
  },
  icons: {
    iconfont: 'mdi', // default - only for display purposes
  },
});
