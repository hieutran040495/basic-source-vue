import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import AppRoutes from './app.router';
import AuthRoutes from './auth.router';
import store from '@/store';
import { i18n, loadLanguageAsync } from '@/plugins/vue-i18n';
import { DEFAULT_LOCALE } from '@/shared/constants/default-locale';
import { LOCALES } from '@/shared/constants/locales';
import LazyLoadService from '@/shared/utils/lazy-load.service';

Vue.use(VueRouter);
const { locale } = i18n;

const routes: RouteConfig[] = [
  {
    path: '/',
    redirect: locale,
  },
  {
    path: '/:lang',
    component: {
      template: '<router-view></router-view>',
    },
    children: [
      ...AppRoutes,
      ...AuthRoutes,
      {
        path: '**',
        component: LazyLoadService.loadView('error-page/404'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  // Setting i18n;
  let lang = to.params.lang;
  const pre_lang = router.currentRoute.params.lang;
  const isValidLang = LOCALES.includes(lang);

  if (!isValidLang) {
    let routePath = to.path;
    if (routePath.charAt(0) === '/') {
      routePath = `${routePath.slice(1)}`;
    }
    // When change route by function
    if (pre_lang) {
      const name = routePath ? routePath : 'home';
      next({ name: name, params: { lang: pre_lang }, replace: true });
    } else {
      // When change route by url
      const path = routePath ? `/${routePath}` : '';
      const next_path = {
        path: `${DEFAULT_LOCALE}${path}`,
        params: { lang: DEFAULT_LOCALE },
        query: { ...to.query },
        replace: true,
      };
      next(next_path);
    }
    return;
  }
  loadLanguageAsync(lang).then(() => next());

  const requiredAuth = to.matched.some((record) => record.meta.requiredAuth);
  if (requiredAuth && !store.getters['auth/isAuthenticated']) {
    next(`/${lang}/auth/login?redirect=${to.path}`);
    return;
  }

  next();
});

export default router;
