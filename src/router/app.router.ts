import { RouteConfig } from 'vue-router';
import LazyLoadService from '@/shared/utils/lazy-load.service';

const AppRoutes: RouteConfig[] = [
  {
    path: '',
    name: 'view',
    component: LazyLoadService.loadView('views'),
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: 'my-page',
        name: 'my-page',
        component: LazyLoadService.loadView('my-page'),
      },
      {
        path: 'users',
        name: 'users',
        component: LazyLoadService.loadView('users'),
      },
      {
        path: 'detail',
        name: 'detail',
        component: LazyLoadService.loadView('detail'),
      },
    ],
  },
];

export default AppRoutes;
