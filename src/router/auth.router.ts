import { RouteConfig } from 'vue-router';
import LazyLoadService from '@/shared/utils/lazy-load.service';
import store from '@/store';

function authGuard(to: any, from: any, next: any) {
  if (store.getters['auth/isAuthenticated']) {
    next('/');
    return;
  }
  next();
}

const AuthRoutes: RouteConfig[] = [
  {
    path: 'auth',
    name: 'auth',
    component: LazyLoadService.loadView('authenticate/auth'),
    beforeEnter: authGuard,
    redirect: 'auth/login',
    children: [
      {
        path: 'login',
        name: 'login',
        component: LazyLoadService.loadView('authenticate/login'),
      },
      {
        path: 'password/forgot',
        name: 'password-forgot',
        component: LazyLoadService.loadView('authenticate/password-forgot'),
      },
      {
        path: 'password/reset',
        name: 'password-reset',
        component: LazyLoadService.loadView('authenticate/password-reset'),
      },
      {
        path: 'register',
        name: 'register',
        component: LazyLoadService.loadView('authenticate/register'),
      },
    ],
  },
];

export default AuthRoutes;
