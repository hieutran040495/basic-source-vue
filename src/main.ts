import './registerServiceWorker';
import './plugins/index';

import Vue from 'vue';
import App from '@/layouts/index.vue';

import vuetify from '@/plugins/vuetify';

import router from './router';
import { i18n } from '@/plugins/vue-i18n';

import store from './store';

import './styles/index.scss';
Vue.config.productionTip = false;

new Vue({
  router,
  i18n,
  store,
  // @ts-ignore
  vuetify,
  render: (h) => h(App),
}).$mount('#app');

export default app;
