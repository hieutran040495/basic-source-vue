import Vue from 'vue';
import { SnotifyService } from 'vue-snotify/SnotifyService';
import { MESSAGE_ERROR } from '@/shared/enums/message-error';

export function handleMessageErrors(errors: any, status?: number): string | undefined {
  switch (errors.constructor) {
    case String:
      return errors;
    case Object:
      if (status === 401) {
        return MESSAGE_ERROR.EMAIL_PASSWORD_INCORRECT;
      }
      if (errors.hasOwnProperty('message')) {
        return errors.message;
      }
    case Array:
      if (errors.length > 0) {
        return errors[0];
      }
  }
}

export default {
  success(msg: string) {
    (Vue.prototype.$snotify as SnotifyService).success(msg);
  },
  error(msg: string) {
    (Vue.prototype.$snotify as SnotifyService).error(msg);
  },
  warning(msg: string) {
    (Vue.prototype.$snotify as SnotifyService).warning(msg);
  },
  info(msg: string) {
    (Vue.prototype.$snotify as SnotifyService).info(msg);
  },
  html(msg: string) {
    (Vue.prototype.$snotify as SnotifyService).html(msg);
  },
};
