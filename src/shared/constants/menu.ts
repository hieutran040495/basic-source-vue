export const MENU = [
  {
    key: 'my_page',
    path: 'my-page',
  },
  {
    key: 'notice',
    path: 'users',
  },
  {
    key: 'inspection_results',
    path: 'detail',
  },
  {
    key: 'diagnosis_and_consultation',
    path: 'consult',
  },
  {
    key: 'account_information',
    path: 'infor',
  },
  {
    key: 'contact_us',
    path: 'contact',
  },
  // {
  //   key: 'log_out',
  //   path: '/',
  // },
];
