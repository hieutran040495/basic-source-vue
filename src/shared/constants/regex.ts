export const REGEXS = {
  whitespace: /^\S+/,
  name_kana: /^[ァ-・ヽヾ゛゜ー]+$/,
  only_number: /^[0-9]\d*$/,
  email: /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/,
  password: /^[a-zA-Z0-9]+$/,
  phone: /^0\d{0,10}$/,
};
