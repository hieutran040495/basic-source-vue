import { Deserializable } from '@/shared/interfaces/deserializable';

export interface PaginationInput {
  current_page: number;
  per_page: number;
  last_page: number;
  total: number;
  total_visible?: number;
}

export class Pagination implements Deserializable<Pagination>, PaginationInput {
  current_page: number;
  per_page: number;
  last_page: number;
  total: number;
  get isPagination(): boolean {
    return this.total > this.per_page;
  }
  max_size?: number;

  constructor() {
    this.deserialize({
      current_page: 1,
      last_page: 1,
      per_page: 20,
      total: 1,
      total_visible: 5,
    });
  }

  deserialize(input: Partial<PaginationInput>): Pagination {
    Object.assign(this, input);
    return this;
  }

  hasJSON() {
    return {
      page: this.current_page,
      per_page: this.per_page,
    };
  }
}
