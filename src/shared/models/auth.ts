import { Deserializable } from '@/shared/interfaces/deserializable';

export interface AuthInput {
  id?: string;
  email?: string;
  password?: string;
  password_confirmation?: string;
  token?: string;
}

export interface AuthSuccessOutput {
  access_token: string;
}

export class Auth implements Deserializable<Auth>, AuthInput {
  id: string;
  email: string;
  password: string;
  password_confirmation: string;
  token: string;

  constructor() {
    this.deserialize({});
  }

  deserialize(input: Partial<AuthInput>): Auth {
    if (input) {
      Object.assign(this, input);
    }
    return this;
  }

  formToJSON() {
    const data: any = {
      password: this.password,
    };

    if (this.id) {
      data.id = this.id;
    }

    if (this.email) {
      data.email = this.email;
    }

    if (this.password_confirmation) {
      data.password_confirmation = this.password_confirmation;
    }

    if (this.token) {
      data.token = this.token;
    }

    return data;
  }
}
