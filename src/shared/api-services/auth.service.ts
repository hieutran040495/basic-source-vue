import Api from './api.service';
import { Auth } from '../models/auth';

class AuthService {
  login(data: Auth): Promise<any> {
    return Api.post('auth/login', data);
  }

  logout() {
    return Api.post('auth/logout');
  }

  forgotPassword(data: {}) {
    return Api.post('password/forgot', data);
  }

  resetPassword(data: Auth) {
    return Api.post('password/reset', data);
  }
}

export default new AuthService();
