import Api from './api.service';

class UserService {
  getUsers(opts?: any) {
    return Api.get('client-users', opts).then((res) => {
      // res.data = res.data.map((user: UserInput) => new User().deserialize(user))
      return res;
    });
  }
}

export default new UserService();
