import Axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import store from '@/store';
import VueSnotify, { handleMessageErrors } from '@/shared/utils/vue-snotify';

function makeHeaders() {
  let headers: any = {
    'Content-Type': 'Application/json',
  };

  if (store.getters['auth/isAuthenticated']) {
    headers = {
      ...headers,
      Authorization: `Bearer ${store.getters['auth/token']}`,
    };
  }

  return headers;
}

function makeRequest(params = {}): AxiosRequestConfig {
  return {
    params,
    headers: makeHeaders(),
  };
}

function handleReqAndResError(errors: AxiosError) {
  const res = errors.response as AxiosResponse;
  const msg = handleMessageErrors(res.data, res.status);

  if (msg) {
    VueSnotify.error(msg);
  }

  return msg;
}
Axios.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    return config;
  },
  (errors: AxiosError) => {
    return Promise.reject(handleReqAndResError(errors));
  },
);

Axios.interceptors.response.use(
  (res: AxiosResponse<any>) => {
    return res.data;
  },
  (errors: AxiosError) => {
    return Promise.reject(handleReqAndResError(errors));
  },
);

export default {
  get(path: string, params = {}) {
    const request = makeRequest(params);
    return Axios.get(path, request);
  },
  post(path: string, data = {}, params = {}) {
    const request = makeRequest(params);
    return Axios.post(path, data, request);
  },
  put(path: string, data = {}, params = {}) {
    const request = makeRequest(params);
    return Axios.put(path, data, request);
  },
  delete(path: string, params = {}) {
    const request = makeRequest(params);
    return Axios.delete(path, request);
  },
};
