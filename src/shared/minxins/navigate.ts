import router from '@/router';
import Vue from 'vue';

export const sekRouter = {
  navigate: function (route: any) {
    router.push(route).catch((err) => {});
  },
};

Vue.mixin({
  methods: sekRouter,
});
