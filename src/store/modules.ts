const requireModule = require.context('.', true, /\.ts$/);
const modules: any = {};

const modulesNotRequired = ['./index.ts', './types.ts', './modules.ts'];

requireModule.keys().forEach((fileName) => {
  if (!modulesNotRequired.includes(fileName)) {
    const path = fileName.replace(/(\.\/|\.ts)/g, '');
    const [moduleName, imported] = path.split('/');

    if (!modules[moduleName]) {
      modules[moduleName] = {
        namespaced: true,
        name: moduleName,
      };
    }

    modules[moduleName][imported] = requireModule(fileName).default;
  }
});

export default modules;
