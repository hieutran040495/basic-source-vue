import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './types';
import modules from './modules';
import authPersistedState from '@/plugins/persisted-state/auth';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store: StoreOptions<RootState> = {
  state: {
    version: '1.0.0',
  },
  actions: {},
  mutations: {},
  getters: {},
  modules: {
    ...modules,
  },
  plugins: [authPersistedState],
  strict: debug,
};
export default new Vuex.Store(store);
