import { MutationTree } from 'vuex';
import { AuthState } from './state';

const mutations: MutationTree<AuthState> = {
  UPDATE_TOKEN(state, payload: any) {
    state.token = payload;
  },
  REMOVE_TOKEN(state) {
    state.token = null;
  },
};

export default mutations;
