export interface AuthState {
  token: string | null;
}

const state: AuthState = {
  token: null,
};

export default state;
