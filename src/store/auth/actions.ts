import { ActionTree } from 'vuex';

import { AuthState } from './state';

import { RootState } from '@/store/types';
import { Auth } from '@/shared/models/auth';

const actions: ActionTree<AuthState, RootState> = {
  updatedToken({ commit }, payload: string) {
    commit('UPDATE_TOKEN', payload);
  },
  userLogout: async ({ commit }) => {
    commit('REMOVE_TOKEN');
    return true;
  },
};

export default actions;
