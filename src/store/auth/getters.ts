import { AuthState } from './state';
import { GetterTree } from 'vuex';
import { RootState } from '@/store/types';

const getters: GetterTree<AuthState, RootState> = {
  token: (state) => state.token,
  isAuthenticated: (state) => !!state.token,
};

export default getters;
