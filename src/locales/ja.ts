import jaMess from './validation-ja.json';

export const ja = {
  common: {
    select_lang: '言語',
    yes: 'はい',
    no: '番号',
  },
  buttons: {
    cancel: 'キャンセル',
    back_home: '家に帰る',
    login: 'ログイン',
    register: '新規会員登録',
    set_password: 'パスワードを設定',
    send_email: 'パスワード再発行メールを送信',
  },
  links: {
    forgot_password: 'パスワードを忘れた方はこちら',
    return_login: 'ログイン画面へ戻る',
  },
  // Form
  fields: {
    email: 'メールアドレス',
    password: 'パスワード',
    password_confirmation: 'パスワード',
  },
  placeholder: {
    email: 'メールアドレスを入力してください',
    password: 'パスワードを入力してください',
    password_confirm: 'パスワードを入力してください',
  },
  validation: jaMess.messages,
  messages_error: {
    email: 'メールの形式が間違っています',
    password: 'パスワードの形式が間違っています',
  },
  // Page
  login: {
    title: 'Sekhmedにログイン',
  },
  register: {
    title: '登録',
  },
  forgot_password: {
    title: 'パスワードを忘れた方',
    text_1: '会員登録時に登録されたメールアドレスを入力してください。',
    text_2: 'パスワード再設定ページへのメールをお送ります。',
    text_3: 'ご入力頂いたメールアドレスに、確認用メールを送信しました。',
    text_4: 'メール本文に記載されているURLをクリックして、<br >パスワード再設定を完了させてください。',
  },
  reset_password: {
    title: 'パスワード再設定',
    text_1: 'パスワードを再設定します。',
    text_2: '6~30文字以内で半角英数字と半角記号を組み合わせてください。',
    text_3: 'パスワードを変更しました',
  },
  header: {
    menus: {
      my_page: 'マイページ',
      notice: 'お知らせ',
      inspection_results: '検査結果',
      diagnosis_and_consultation: 'オンライン診断・相談',
      account_information: 'アカウント情報',
      contact_us: 'お問い合わせ',
      log_out: 'ログアウト',
    },
  },
};
