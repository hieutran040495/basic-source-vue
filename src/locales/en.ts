import enMess from './validation-en.json';

export const en = {
  common: {
    select_lang: 'Language',
    yes: 'Yes',
    no: 'No',
  },
  buttons: {
    cancel: 'Cancel',
    back_home: 'Back to home',
    login: 'Login',
    register: 'Register',
    set_password: 'Set password',
    send_email: 'Send password reissue email',
  },
  links: {
    forgot_password: 'If you forgot your password, click here',
    return_login: 'Return to login screen',
  },
  // Form & validate
  fields: {
    email: 'Email',
    password: 'Password',
    password_confirmation: 'Password Confirm',
  },
  placeholder: {
    email: 'Please enter your email',
    password: 'Please enter your password',
    password_confirm: 'Enter new password (for confirmation)',
  },
  validation: enMess.messages,
  messages_error: {
    email: 'Email is wrong format',
    password: 'Password is wrong format',
  },
  // Page
  login: {
    title: 'Login to Sekhmed',
  },
  register: {
    title: 'Register',
  },
  forgot_password: {
    title: 'Forgot password',
    text_1: 'Please enter the email address registered when you registered.',
    text_2: 'An email will be sent to the password reset page.',
    text_3: 'A confirmation email has been sent to the email address you entered.',
    text_4: 'Click the URL described in the email body, <br>complete the password reset.',
  },
  reset_password: {
    title: 'Reset password',
    text_1: 'Reset the password.',
    text_2: 'Please combine half-width alphanumeric characters and half-width symbols within 6 to 30 characters.',
    text_3: 'Password changed',
  },
  header: {
    menus: {
      my_page: 'My page',
      notice: 'Notice',
      inspection_results: 'Inspection results',
      diagnosis_and_consultation: 'Diagnosis & consultation',
      account_information: 'Account information',
      contact_us: 'Contact Us',
      log_out: 'Log out',
    },
  },
};
